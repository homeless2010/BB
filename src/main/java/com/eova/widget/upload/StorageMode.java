package com.eova.widget.upload;


public enum StorageMode {
    Local(0),
    QiNiu(1),
    AliOss(2);

    private final Integer storageMode;

    private StorageMode(Integer mode) {
        this.storageMode = mode;
    }

    public Integer getMode() {
        return this.storageMode;
    }
}
