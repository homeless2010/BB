<%
//图片或者文件 三个路径  1、基本路径 http://127.0.0.1:8080/file 2、内部路径如：/avatar/ ,3、文件名 1475647037704.jpg ,需要改成数据库存储：/avatar/1475647037704.jpg 因为内部路径可以能程序调整发生变化
//一个完整的访问路径：http://127.0.0.1:8080/file/avatar/1475647037704.jpg
var fileNameKey = id!+'_name';
//文件真的原名
var srcfileName = null;
if(data! != null)
	srcfileName = @data.get(fileNameKey);

if(isEmpty(filedir!)){
	filedir = "/"+objectCode!+"/"+name!;
}

var uploadDir = filedir;

var imgUrls = "";//多个逗号分隔
if(!isEmpty(value!)){
	
	imgUrls = value!;
	debug(imgUrls);
}

var verify = "";
if(isTrue(isNoN!) ){
	verify = "required=true";
}
if(!isEmpty(validator!)){
	if(verify != ''){
		verify += ';';
	}
	verify = verify + validator!;
}
var disabled="";
if( isTrue(isReadonly!) ||isTrue(view!)){
	disabled = 'disabled=""';
	verify = '';
}

var fileTypes = "";
if(!isEmpty(item.config.fileTypes!)){
	fileTypes = item.config.fileTypes!;
}else
	fileTypes = @com.eova.common.utils.io.FileUtil.IMG_TYPE;
var maxLength = null;
if(!isEmpty(item.config.maxLength!)){
	maxLength = item.config.maxLength!;
}else
	maxLength = @com.eova.config.EovaConst.UPLOAD_IMG_SIZE;

var fileNum= item.config.fileNum!;
if(isEmpty(fileNum!))
	fileNum = 1;

var isMultiple = '';
var multipleTip = '';
if(fileNum != 1){
	isMultiple = 'multiple';
	if(isEmpty(placeholder!))
		multipleTip = '请选择至多'+fileNum+'个文件';
	else
		multipleTip = placeholder!;
}

value = isEmpty( value! ) ? defaultValue!:value!;
%>


 

<!--dom结构部分 http://fex.baidu.com/webuploader/getting-started.html
https://my.oschina.net/lemos/blog/2998818
https://www.helloweba.net/demo/2016/webuploader/
https://blog.csdn.net/caiyongshengcsdn/article/details/79412933
http://www.bootstrap-fileinput.com/options.html

 <div class="bb-img-upload">
 <input id="fileinput_${name!}"  name="fileinput_${id!}" type="file" multiple data-min-file-count="1">
 <input type="hidden"  id="${id!}" name="${name!}" value="${value!}" ${ismultiple!}  lay-verify="${verify!}" title='${item.cn!}'/>
</div>          
-->
<div class="bb-img-upload" >
            <div class="file-loading">
                <input  id="fileinput_${id!}"  name="fileinput_${name!}" type="file" ${isMultiple} data-msg-placeholder="${multipleTip}" ${disabled!}>
            </div>
            <div id="${id!}_show_kv-avatar-errors" class="center-block" style="width:100%;display:none"></div>
            <input type="hidden"  id="${id!}" name="${name!}" value="${value!}" placeholder="${placeholder!}" bb-verify="${verify!}" ${strutil.replace(verify!,';',' ')} target='${id!}_show_kv-avatar-errors'/>
            <input type="hidden"  id="${id!}_num" name="${name!}_num" value="0"  bb-verify="max=${fileNum}" limitFileMax='${fileNum}' placeholder="${multipleTip}"  target='${id!}_show_kv-avatar-errors'/>
            
</div>
        

<script>
$(document).ready(function () {
	//类似 .jpg|.gif|.png|.bmp=>gif,jpg,jpeg,bmp,png
	var fileTypes = "${fileTypes!}".replaceAll('\\.', '').replaceAll('\\|', ',');

	var sourceImgsStr = '${imgUrls!}';
	//let fileUrl $.formResUrl(FILE,sourceFileUrl);
	var initialPreviews = new Array();
	var initialPreviewConfigs = new Array();
	
	let totalImgNum = 0;
	if(sourceImgsStr !=''){
		let sourceImgs = sourceImgsStr.split(',');
		
		for(let i =0;i<sourceImgs.length;i++){
			let oneValue = sourceImgs[i];
			let imgUrl = $.formResUrl(IMG,oneValue);
			
			initialPreviews[i] = imgUrl;
			
			let imgName = $.getFilePathName(sourceImgs[i]);//结尾名（后面从系统文件表提取文件名）
			initialPreviewConfigs[i] = { caption: imgName,filename: imgName,downloadUrl:imgUrl,url:'toDelete',key:oneValue};//type 后面控件会根据后缀自行添加
			
			totalImgNum++;
		}

		//initialPreviewConfigs[0] = {caption: "abc.jpg", type: "jpg"};
		//initialPreviewConfigs[0] ={ caption: "菲哥.jpg", type: "jpg", size: 12345 };
		
		 $('#${id!}_num').attr('value', totalImgNum); 
	}
	
	<% if(!isEmpty( placeholder! )){ %>
	var tips = '${placeholder!}';
	<% }else{ %>
	var tips = '请选择(支持格式：'+fileTypes+')';
	<% } %>
	
	let showCaption =  ${fileNum != 1?true:false};
	let showBrowse = ${fileNum != 1?true:false};
	
	$("#fileinput_${id!}").fileinput({
		language: 'zh', //设置语言
		<% if(fileNum != 1){ %>
		 theme: "explorer-fa",   // 主题(数量多直接换成小行模式)
		<% } %>   
	    uploadUrl: ctx+'upload/file?name=fileinput_${name!}&filedir=${filedir}', //上传的地址
	    overwriteInitial: false,
	    maxFileSize: ${maxLength!},//这个是单个文件大小
	    
	    maxTotalFileCount:${fileNum},
	    msgTotalFilesTooMany:'超过限定的文件数量:{m}!',
	    
	    showClose: false,
	    showCaption: showCaption,
	    showBrowse: showBrowse,
	    initialPreviewAsData: true,
	    browseOnZoneClick: true,
	    removeFromPreviewOnError:true, //当选择的文件不符合规则时，例如不是指定后缀文件、大小超出配置等，选择的文件不会出现在预览框中，只会显示错误信息  
	    
	    initialPreview: initialPreviews,
	    initialPreviewAsData: true, // defaults markup  
	    initialPreviewConfig: initialPreviewConfigs,
	    
	    previewFileIconSettings:{
			'docx':'<i class="glyphicon glyphcion-file"></i>',
			'xlsx':'<i class="glyphicon glyphcion-file"></i>',
			'pptx':'<i class="glyphicon glyphcion-file"></i>',
			'jpg':'<i class="glyphicon glyphcion-picture"></i>',
			'pdf':'<i class="glyphicon glyphcion-file"></i>',
			'zip':'<i class="glyphicon glyphcion-file"></i>',
		},

	    
	    removeLabel: '删除',
	    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
	    removeTitle: 'Cancel or reset changes',
	  //  elErrorContainer: '#${id!}_show_kv-avatar-errors',
	    //msgErrorClass: 'alert alert-block alert-danger',
	    <% if(fileNum == 1){ %>
	    defaultPreviewContent: '<img src="/ui/images/upload-a.png" alt="积木文件上传" alt=""><h6 class="text-muted">'+tips+'</h6>',
	    <% } %>
	    layoutTemplates: {main2: '{preview} ' },
	    allowedFileExtensions : fileTypes.split(","),//接收的文件后缀（数组）
	    
	    //otherActionButtons: btns,
	});

	var filesMap = new Map();
	$("#fileinput_${id!}").on("fileuploaded", function (event, data, previewId, index) {
	
		 if(data.response.success == true){
			 console.log(filesMap);
			 filesMap.set(previewId,data.response.fileName)
	         //alert('上传成功');
	     	//fileName: "http://img.bblocks.cn/c159e1c8032344788e45eb3904b3fc1e.jpg"
	     	//	msg: "上传成功"
	     	let existUrl = $('#${id!}').val();
	        if(existUrl == ''){
	        	$('#${id!}').attr('value', data.response.fileName); 
	        	
	        }else{
	        	$('#${id!}').attr('value', existUrl+','+data.response.fileName); 
	        }
	     	
			 
	        $('#${id!}_num').attr('value', parseInt($('#${id!}_num').val())+1); 
			 //判断是否存在文件名字段吗（不存在这个业务了）
			 /**
			 if ( $("#${fileNameKey!}").length > 0 ) { 
				 $("#${fileNameKey!}").val(data.files[0].name); 
			 } **/
			 
	     }else{
	     	//data.response.msg;
	     	
	     }
	
	 }).on("filebatchselected", function(event, files) {
	    $(this).fileinput("upload");
	 
	 }).on('filesuccessremove', function (event, previewId, extra) {
		 console.log('开始删除。。。');
		 filesMap.delete(previewId);
	　　　　　　//在移除事件里取出所需数据，并执行相应的删除指令
		   //delete(($('#' + previewId).attr('fileid'));
		 $('#${id!}_num').attr('value', parseInt($('#${id!}_num').val())-1);
	 }).on('filedeleted', function(event, key, jqXHR, data) {
		    console.log('Key = ' + key);
		    $('#${id!}_num').attr('value', parseInt($('#${id!}_num').val())-1);
	 });
});
</script>